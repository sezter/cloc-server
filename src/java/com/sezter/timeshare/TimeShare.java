/**
 * REST Service allowing TimeShare user instances to:
 * Set up accounts
 * Share clocks
 * Notify of time changes
 */
package com.sezter.timeshare;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;

/**
 * REST Web Service
 *
 * @author Sez Prouting & Terry Green
 */
@Path("timeshare")
public class TimeShare {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of TimeShare
     */
    public TimeShare() {
    }

    /**
     * Retrieves representation of an instance of com.sezter.timeshare.TimeShare
     * @param name is the name of the person we are greeting
     * @return an instance of java.lang.String which greets the person by name
     */
    @GET
    //@Produces(MediaType.APPLICATION_XML)
    @Path("{name}")
    @Consumes("text/plain")
    @Produces("text/plain")
    public String getXml(@PathParam("name") String name) {
        //return "<?xml version=\"1.0\" encoding=\"utf-8\"?>";
        //return "<HTML><BODY><H1>Hello Sezter</H1><P>This is your rest service</P></BODY></HTML>";
        return "Hello " + name;
    }

    /**
     * PUT method for updating or creating an instance of TimeShare
     * @param content representation for the resource
     */
    @PUT
    @Consumes(MediaType.APPLICATION_XML)
    public void putXml(String content) {
    }
}
